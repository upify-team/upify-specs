#
# Be sure to run `pod lib lint UPKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UPKit'
  s.version          = '1.3.17'
  s.summary          = 'Upify iOS Framework with some core functionalities'
# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Upify iOS Framework with some core functionalities

 - APIClient
 - APIRoutes
 - APIError
 - UITableView+Utils
 - SessionManager
 - Environments
 - BaseTableViewController

                       DESC

  s.homepage         = 'https://bitbucket.org/upify-team/upkit'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'diegof29' => 'diego.pais29@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/upify-team/upkit.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.source_files = 'UPKit/Classes/**/*'
  
  # s.resource_bundles = {
  #   'UPKit' => ['UPKit/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.dependency 'Alamofire', '~> 4.7.3'
  s.dependency 'ObjectMapper', '~> 3.4.2'
  s.dependency 'SwiftyJSON', '~> 4.0.0'
  s.dependency 'KeychainAccess', '~> 3.1'
  s.dependency 'IQKeyboardManagerSwift', '~> 6.0.5'

end
